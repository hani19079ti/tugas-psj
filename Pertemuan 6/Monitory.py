import time, concurrent.futures, subprocess, threading
from datetime import datetime

def print_message(args = True, host = 'none'):
    now = datetime.now()
    dt_string = now.strftime("%Y-%m-%d %H:%M:%S")
    file = open('report-monitor.csv',"a")
    if args == 'Up' or args == 'Down':
        result_detail = dt_string + ';' + host + ';' + args
        print(result_detail)
        file.write(result_detail + '\n')
    elif invalidIp:
        print('Alamat ip salah')
    file.close()

def check_host(arg):
    p1 = subprocess.Popen(['ping','-c3',arg],stdout=subprocess.PIPE)
    p2 = subprocess.Popen(['grep','packet'],stdin=p1.stdout,stdout=subprocess.PIPE)
    p1.stdout.close()
    output = p2.communicate()[0].decode('utf-8').split(",")
    if len(output) == 5:
        print_message(args='Down',host=arg)
    else :
        if int(output[2][1]) >= 1:
            print_message(args='Down',host=arg)
        else:
            print_message(args='Up',host=arg)

def run_check_host():
    T1 = time.perf_counter()
    print('\nmonitoring mulai....\n')
    with concurrent.futures.ThreadPoolExecutor() as executor:
        file = open('host.cfg','r')
        ips = file.readlines()
        results = [executor.submit(check_host,ip.strip()) for ip in ips]
        for f in concurrent.futures.as_completed(results):
            f.result()
    T2 = time.perf_counter()
    print(f"\nmonitoring selesai dalam waktu : {round(T2 - T1, 2)} detik...")


WAIT_TIME_SECOND = 20
ticker = threading.Event()
if __name__ == "__main__":
    try:
        while not ticker.wait(WAIT_TIME_SECOND):
            run_check_host()
    except :
        ticker.clear()
        print('\nclose program')
